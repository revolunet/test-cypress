import Document, { Head, Main, NextScript } from "next/document";
import { injectGlobal, ServerStyleSheet } from "styled-components";

injectGlobal`
  * {
    font-family:  "Opensans Regular", "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", Geneva, Verdana, sans-serif
  }
  html {
    min-height:100%;
  }
  body {
    margin: 0;
    padding: 0;
    color: #222;
    font-size: 0.9em;
  }
  table {
    padding: 0;
    border-collapse: collapse;
    min-width: 50vh;
  }
  table th {
    padding: 3px;
    border: 1px solid silver;
    background: #eee;
  }
  table td {
    padding: 3px;
    border: 1px solid silver;
  }

`;

export default class MyDocument extends Document {
  static getInitialProps({ renderPage }) {
    const sheet = new ServerStyleSheet();
    const page = renderPage(App => props => sheet.collectStyles(<App {...props} />));
    const styleTags = sheet.getStyleElement();
    return { ...page, styleTags };
  }

  render() {
    return (
      <html>
        <Head>
          <title>F.A.Q. Code du travail</title>
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
          <link
            rel="shortcut icon"
            type="image/x-icon"
            href={this.props.__NEXT_DATA__.assetPrefix + "/static/assets/favicon.ico"}
          />
          {this.props.styleTags}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
