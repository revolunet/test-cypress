const withImages = require("next-images");

// si on est hebergé sur github.io on est dans un sous-dossier...
const isProduction = process.env.NODE_ENV === "production";
const baseUrl = isProduction ? "/faq-code-du-travail/" : "";

module.exports = withImages({
  exportPathMap: function() {
    return {
      "/": { page: "/" }
    };
  },
  assetPrefix: baseUrl,
  webpack: config => {
    config.output.publicPath = baseUrl;
    return config;
  }
});
