import styled from "styled-components";
import { DebounceInput } from "react-debounce-input";

export const SearchInput = styled(DebounceInput)`
  font-size: 1em;
  padding: 10px 5px;
  text-align: left;
  border-radius: 1px;
  margin: 0 auto;
  margin-top: 20px;
  width: 100%;
  box-sizing: border-box;
  max-width: 600px;
  display: block;
  border: 1px solid silver;
`;

export const PageTitle = styled.div`
  font-size: 1.5em;
  text-align: center;
  margin: 0;
  text-decoration: underline;
`;

export const SommaireTitle = styled.div`
  font-weight: bold;
  font-size: 1.4em;
  margin-bottom: 10px;
  @media screen and (max-width: 700px) {
    font-size: 1.3em;
  }
`;

export const SommaireEntries = styled.div`margin: 0;`;

export const SommaireEntry = styled.div`
  cursor: pointer;
  line-height: 2em;
  overflow: hidden;
  height: 2em;
  text-overflow: ellipsis;
  white-space: nowrap;
  &::before {
    content: "●";
    margin-right: 5px;
    color: #ddd;
  }
  &:hover {
    &::before {
      color: black;
    }
  }

  ${props =>
    props.selected
      ? `
    font-weight: bold;
     &::before {
      color: black;
    }
    `
      : ``};
`;

export const ResultsTitle = styled.div`
  text-align: center;
  font-size: 1.6em;
  line-height: 2em;
  @media screen and (max-width: 700px) {
    margin-top: 20px;
  }
`;

export const Block = styled.div`
  padding: 20px;
  background: #fafafa;
  border: 1px solid silver;
  color: black;
  border-radius: 1px;
  box-sizing: border-box;
  @media screen and (max-width: 700px) {
    padding: 10px;
  }
`;

// TODO: remove hardcoded path ! (gh-pages)
const isProduction = process.env.NODE_ENV === "production";
const baseUrl = isProduction ? "/faq-code-du-travail" : "";

export const Marianne = styled.div`
  width: 100%;
  height: 100px;
  margin: 0 auto;
  text-align: center;
  background-image: url(${baseUrl}/static/assets/marianne.png)});
  background-repeat: no-repeat;
  background-position: center center;
  background-size: contain;
  @media screen and (max-width: 700px) {
    max-width: 150px;
    max-height: 150px;
  }
`;

export const Container = styled.div`
  padding: 20px;
  max-width: 1200px;
  margin: 0 auto;
  @media only screen and (max-width: 700px) {
    padding: 10px;
  }
`;

const capitalize = string => {
  string = string.toLowerCase();
  return string.charAt(0).toUpperCase() + string.substring(1);
};

export const WelcomeText = () => (
  <Block style={{ padding: "10px 30px", textAlign: "center" }}>
    <h2>F.A.Q. du code du travail</h2>
    <p style={{ fontSize: "1.2em", lineHeight: "1.5em" }}>
      55 réponses aux questions les plus fréquemment posées
    </p>
  </Block>
);

export const Sommaire = ({ className, title, selected, entries, onClick }) => (
  <div style={{ marginTop: 20 }} className={className}>
    <SommaireTitle>{title}</SommaireTitle>
    <SommaireEntries>
      {entries.map(entry => (
        <SommaireEntry
          selected={selected === entry}
          key={entry}
          onClick={() => onClick(entry)}
        >
          {capitalize(entry)}
        </SommaireEntry>
      ))}
    </SommaireEntries>
  </div>
);
