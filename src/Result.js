import styled from "styled-components";
import highlighter from "document-highlighter";

highlighter.defaultOptions.before = `<span style="background:yellow">`;
highlighter.defaultOptions.after = "</span>";
highlighter.defaultOptions.language = "fr";

export const Contexte = styled.div`
  font-size: 1.3em;
  font-weight: bold;
`;

export const Question = styled.div`
  font-size: 1.2em;
  font-weight: bold;
  margin: 15x 0 20px 0;
  line-height: 1.5em;
  border-left: 1px solid silver;
  padding-left: 20px;
  padding-right: 20px;
`;

export const Reponse = styled.div`
  font-size: 1.1em;
  line-height: 1.6em;
  text-align: justify;
`;

const HightLightDiv = ({ text, query, style }) => (
  <div
    style={style}
    dangerouslySetInnerHTML={{ __html: highlighter.html(text || "", query).html }}
  />
);

const BlockResult = styled.div`
  padding: 20px;
  border-bottom: 1px solid silver;
`;

const GoUp = ({ onClick }) => (
  <div style={{ textAlign: "right", marginTop: 30 }}>
    <div
      style={{ display: "inline-block", cursor: "pointer", textDecoration: "underline" }}
      onClick={onClick}
    >
      ⬆ Retour en haut
    </div>
  </div>
);

const Result = ({
  style,
  showTheme,
  showBranche,
  showGoUp,
  onGoUp,
  question,
  reponse,
  theme,
  branche,
  query
}) => {
  const categoryStyle = { margin: "10px 0", fontWeight: "normal", fontSize: "0.8em" };
  return (
    <BlockResult style={style}>
      <Contexte>
        {showTheme && (
          <HightLightDiv style={categoryStyle} text={`<b>Thème :</b> ${theme}`} query={query} />
        )}
        {showBranche && (
          <HightLightDiv style={categoryStyle} text={`<b>Branche :</b> ${branche}`} query={query} />
        )}
      </Contexte>
      <Question>{<HightLightDiv text={question} query={query} />}</Question>
      <Reponse>
        <HightLightDiv text={reponse} query={query} />
      </Reponse>
      {showGoUp && <GoUp onClick={onGoUp} />}
    </BlockResult>
  );
};

export default Result;
