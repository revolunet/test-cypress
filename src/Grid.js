import ReactDOM from "react-dom";
import styled from "styled-components";
import { Block } from "./components";

// CSS grid layouts
const layout = `
  "menu   infos  "
  "menu resultats"
  "menu resultats"
  "menu resultats"
`;

const layoutMobile = `
  "menu"
  "infos"
  "resultats"
`;

export const Grid = styled.div`
  display: grid;
  grid-row-gap: 5px;
  grid-column-gap: 5px;
  grid-template-columns: 2fr 5fr;
  xgrid-auto-columns: 100%;
  height: auto;
  grid-template-areas: ${layout};
  @media only screen and (max-width: 700px) {
    grid-template-areas: ${layoutMobile};
    grid-template-columns: 1fr;
    font-size: 0.8em;
    grid-row-gap: 5px;
    grid-column-gap: 5px;
  }
`;

export const Intro = styled.div`grid-area: intro;`;

export const Menu = styled(Block)`
  grid-area: menu;
  background-color: #efefef;
`;

export const StyledResultats = styled.div`
  grid-area: resultats;
  overflow: scroll;
  height: calc(100vh - 100px);
  padding-right: 20px;
  grid-row: 2 / span 5;
  @media only screen and (max-width: 700px) {
    grid-row: auto;
    overflow: auto;
    height: 100%;
    padding-right: 0;
  }
`;

// force  scrollTop reset
export class Resultats extends React.Component {
  componentDidUpdate() {
    this.resetScroll();
  }
  resetScroll = () => {
    if (typeof window !== "undefined") {
      window.scrollTo(0, 0);
    }
    const node = ReactDOM.findDOMNode(this);
    if (node) {
      node.scrollTop = 0;
    }
  };
  render() {
    return (
      <StyledResultats {...this.props} ref={node => (this.node = node)}>
        {this.props.render({ resetScroll: this.resetScroll })}
      </StyledResultats>
    );
  }
}
